package com.example.paises.enetity;

import java.io.Serializable;

public class pais implements Serializable {
    String nombre;
    String himno;
    String url;
    String videoUrl;

    public pais(String nombre,String videoUrl ,String himno, String url) {
        this.nombre = nombre;
        this.himno = himno;
        this.url = url;
        this.videoUrl = videoUrl;
    }

    public String getHimno() {
        return himno;
    }

    public void setHimno(String himno) {
        this.himno = himno;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
