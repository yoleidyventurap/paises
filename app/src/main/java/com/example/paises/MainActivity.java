package com.example.paises;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.paises.Recycler.adaptador;
import com.example.paises.Repository.Datos;
import com.example.paises.enetity.pais;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<pais> ListDatos;
    ArrayList<pais> ListDatos2;
    Datos datos=new Datos();
    RecyclerView recycler;
    Button btn;
    FirebaseFirestore db;
    Activity activity;
    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db=FirebaseFirestore.getInstance();
        ListDatos=new ArrayList<pais> ();




        recycler=(RecyclerView) findViewById(R.id.recycler1);
        consultar();
        getAll();

        recycler.setLayoutManager(new LinearLayoutManager(this));
        adaptador adapter=new adaptador(ListDatos, this);

        recycler.setAdapter(adapter);

    }


    public ArrayList<pais> consultar(){
        Toast.makeText(this,"Evento",Toast.LENGTH_SHORT).show();
        Datos datos=new Datos();


        db.collection("Paises")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<pais> types = queryDocumentSnapshots.toObjects(pais.class);

                        ListDatos.addAll(types);

                        System.out.println(ListDatos.toString());


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });



        return ListDatos2;
    }

    private void getAll() {
        ListDatos=datos.llenarDatos();
        if(ListDatos.isEmpty()){
            Toast.makeText(this,"Vacio",Toast.LENGTH_SHORT).show();
        }else{

        }
    }

}
